const exception = require('./lib/exception')
const utils = require('./lib/utils')
const errorMessages = require('./lib/middlewares/error_messages')

require('./i18n')('es')
module.exports = {
  houndsExceptions: exception,
  houndsUtils: utils,
  middlewares: {
    errorMessages
  }
}
