class ExtendableError extends Error {
  constructor (code, description) {
    super(description)
    this.code = code
    this.name = this.constructor.name
    this.description = description
    Error.captureStackTrace(this, this.constructor.name)
  }
}

function getExceptionDescription (description) {
  return description
}

class InternalErrorException extends ExtendableError {
  constructor (description) {
    super(500, getExceptionDescription(description))
  }
}

class BadRequestException extends ExtendableError {
  constructor (description) {
    super(400, getExceptionDescription(description))
  }
}

class UnauthorizedException extends ExtendableError {
  constructor (description) {
    super(401, getExceptionDescription(description))
  }
}

class PaymentException extends ExtendableError {
  constructor (description) {
    super(600, getExceptionDescription(description))
  }
}

class ExpiredException extends ExtendableError {
  constructor (description) {
    super(424, getExceptionDescription(description))
  }
}

module.exports = {
  InternalErrorException,
  BadRequestException,
  UnauthorizedException,
  PaymentException,
  ExpiredException
}
