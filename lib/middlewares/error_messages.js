const validation = require('express-validation')
const mongoose = require('mongoose')
const i18n = require('i18n')
const { InternalErrorException, BadRequestException, UnauthorizedException, PaymentException, ExpiredException } = require('../exception')

module.exports = (err, req, res, next) => {
  let message
  if (err instanceof InternalErrorException || err instanceof BadRequestException ||
    err instanceof UnauthorizedException || err instanceof PaymentException ||
    err instanceof ExpiredException) {
    return next(err)
  }
  if (err instanceof mongoose.Error.ValidationError) {
    message = Object.keys(err.errors).map(key => {
      if (err.errors[key].properties && err.errors[key].properties.type === 'unique') {
        return i18n.__('Unique Constraint', err.errors[key].properties.path, { email: err.errors[key].properties.value })
      }
      return err.errors[key].message
    }).join(` ${i18n.__('and')} `)
    if (message.indexOf('is not a valid enum value for path `gender`') >= 0) {
      message = 'is not a valid enum value for path gender'
    }
    return next(new BadRequestException(message))
  }
  if (err instanceof validation.ValidationError) {
    message = err.errors.map(error => error.messages.join('. ')).join(' and ')
    return next(new BadRequestException(message))
  }
  return next(new InternalErrorException(err.message))
}
