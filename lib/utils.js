const moment = require('moment-timezone')
const i18n = require('i18n')

function arrayToObjectForSelect (array) {
  return array.reduce((result, option) => {
    result[option.trim()] = true
    return result
  }, {})
}

function arrayToObjectForSort (array) {
  const { result } = array.reduce((obj, sort) => {
    let { splitResult, order, result } = obj
    splitResult = sort.trim().split(' ')
    order = splitResult[1] ? splitResult[1].trim() : 'asc'
    order = order === 'asc' ? 1 : -1
    result[splitResult[0].trim()] = order
    return obj
  }, { result: {}, order: {}, splitResult: [] })
  return result
}

const string = {
  deleteSpaces (string) {
    if (string) {
      return string.replace(/\s/g, '')
    }
    return null
  }
}

const format = {
  isEmail (email) {
    if (!email) {
      return false
    }
    const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ // eslint-disable-line no-useless-escape
    return emailPattern.test(email)
  }
}

const paginate = {
  limit (limit) {
    return limit ? Number(limit) : 20
  },
  page (page) {
    return (page && page > 0) ? Number(page) : 1
  },
  select (value) {
    if (!value) {
      return {}
    }
    if (typeof value === 'string') {
      const selectArray = value.split(',')
      return arrayToObjectForSelect(selectArray)
    }
    if (value instanceof Array) {
      return arrayToObjectForSelect(value)
    }
    return {}
  },
  sort (value) {
    if (!value) {
      return {}
    }
    if (typeof value === 'string') {
      const selectArray = value.split(',')
      return arrayToObjectForSort(selectArray)
    }
    if (value instanceof Array) {
      return arrayToObjectForSort(value)
    }
    return {}
  }
}

const date = {
  age (birthdate, format) {
    format = format || 'llll'
    return moment().diff(moment(birthdate, format), 'years')
  },
  time (timeString, format) {
    format = format || 'HH:mm a'
    return moment.utc(timeString, format)
  },
  dateTime (timeString, format) {
    return moment.utc(timeString, format)
  },
  weekdays (day) {
    return i18n.__(moment.weekdays(day))
  },
  month (date, format) {
    return moment(date, format).month() + 1
  },
  now () {
    return moment()
  },
  firstDay (options) {
    const now = new Date()
    const year = options.year || now.getFullYear()
    const month = options.month || now.getMonth() + 1
    const firstDay = new Date(Date.UTC(year, month - 1, 1, 0, 0, 0, 0))
    return moment(firstDay)
  },
  lastDay (options) {
    const now = new Date()
    const year = options.year || now.getFullYear()
    const month = options.month || now.getMonth() + 1
    const lastDay = new Date(Date.UTC(year, month, 0, 23, 59, 59, 0))
    return moment.utc(lastDay)
  },
  birthDate (age) {
    const now = moment()
    return now.subtract(age, 'y')
  }
}

module.exports = {
  string,
  format,
  date,
  paginate
}
