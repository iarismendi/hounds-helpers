const test = require('ava')
const utils = require('../lib/utils')
const moment = require('moment-timezone')

test.serial('String#deleteSpaces', t => {
  const { string: stringUtil } = utils
  t.truthy(stringUtil.deleteSpaces, 'deleteSpaces should exist')
  const test = ' test test '
  const result = 'testtest'
  t.deepEqual(stringUtil.deleteSpaces(test), result, 'should be the same')
  t.falsy(stringUtil.deleteSpaces(), 'should not exist')
})

test.serial('format#isEmail', t => {
  let isEmail
  const { format: formatUtil } = utils
  t.truthy(formatUtil.isEmail, 'isEmail should exist')
  isEmail = formatUtil.isEmail('iarismendi@houndssuite.com')
  t.truthy(isEmail, 'should be true')
  isEmail = formatUtil.isEmail('iarismendi@houndssuite..com')
  t.falsy(isEmail, 'should be false')
})

test.serial('paginate#limit', t => {
  const { paginate: paginateUtil } = utils
  t.truthy(paginateUtil.limit, 'limit should exist')
  const defaultLimit = 20
  const limit = '2'
  const result = paginateUtil.limit(limit)
  t.deepEqual(Number(limit), result, 'should be the same')
  t.deepEqual(defaultLimit, paginateUtil.limit(), 'should be the same')
})

test.serial('paginate#page', t => {
  const { paginate: paginateUtil } = utils
  t.truthy(paginateUtil.page, 'page should exist')
  const defaultPage = 1
  const page = '2'
  const result = paginateUtil.page(page)
  t.deepEqual(Number(page), result, 'should be the same')
  t.deepEqual(defaultPage, paginateUtil.page(), 'should be the same')
})

test.serial('paginate#select', t => {
  let test, result
  const expected = { name: true, lastname: true }
  const { paginate: paginateUtil } = utils
  t.truthy(paginateUtil.select, 'select should exist')

  test = 'name, lastname'
  result = paginateUtil.select(test)
  t.deepEqual(expected, result, 'should be the same')

  test = ['name', 'lastname']
  result = paginateUtil.select(test)
  t.deepEqual(expected, result, 'should be the same')

  result = paginateUtil.select()
  t.deepEqual({}, result, 'should be the same')
})

test.serial('paginate#sort', t => {
  let test, result
  const expected = { name: 1, lastname: -1 }
  const { paginate: paginateUtil } = utils
  t.truthy(paginateUtil.sort, 'sort should exist')

  test = 'name, lastname desc'
  result = paginateUtil.sort(test)
  t.deepEqual(expected, result, 'should be the same')

  test = ['name', 'lastname desc']
  result = paginateUtil.sort(test)
  t.deepEqual(expected, result, 'should be the same')

  result = paginateUtil.sort()
  t.deepEqual({}, result, 'should be the same')
})

test.serial('date#age', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.age, 'age should exist')
  const result = dateUtil.age('12/07/1991', 'DD/MM/YYYY')
  t.is('number', typeof result, 'should be the same')
})

test.serial('date#time', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.time, 'time should exist')
  const expected = '14:30'
  const result = dateUtil.time('2:30 pm')
  t.true(result instanceof moment, 'should be true')
  t.deepEqual(expected, result.format('HH:mm'), 'should be the same')
})

test.serial('date#dateTime', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.dateTime, 'dateTime should exist')
  const expected = '1991-07-12T14:30:00.000Z'
  const result = dateUtil.dateTime('12/07/1991 02:30 pm', 'DD/MM/YYYY hh:mm a')
  t.true(result instanceof moment, 'should be true')
  t.deepEqual(expected, result.toISOString(), 'should be the same')
})

test.serial('date#weekdays', t => {
  require('../i18n')('es')
  const { date: dateUtil } = utils
  t.truthy(dateUtil.weekdays, 'weekdays should exist')
  const expected = 'Miércoles'
  const result = dateUtil.weekdays(3)
  t.deepEqual(expected, result, 'should be the same')
})

test.serial('date#month', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.month, 'month should exist')
  const expected = 7
  const result = dateUtil.month('1991-07-12')
  t.deepEqual(expected, result, 'should be the same')
})

test.serial('date#now', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.now, 'now should exist')
  const expected = new Date()
  const result = dateUtil.now()
  t.true(result instanceof moment, 'should be true')
  t.deepEqual(expected.toISOString(), result.toISOString(), 'should be the same')
})

test.serial('date#firstDay', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.firstDay, 'firstDay should exist')
  const expected = '1991-07-01T00:00:00.000Z'
  const result = dateUtil.firstDay({year: 1991, month: 7})
  t.true(result instanceof moment, 'should be true')
  t.deepEqual(expected, result.toISOString(), 'should be the same')
})

test.serial('date#lastDay', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.lastDay, 'lastDay should exist')
  const expected = '1991-07-31T23:59:59.000Z'
  const result = dateUtil.lastDay({year: 1991, month: 7})
  t.true(result instanceof moment, 'should be true')
  t.deepEqual(expected, result.toISOString(), 'should be the same')
})

test.serial('date#birthDate', t => {
  const { date: dateUtil } = utils
  t.truthy(dateUtil.birthDate, 'birthDate should exist')
  const result = dateUtil.birthDate(26)
  t.true(result instanceof moment, 'should be true')
})
