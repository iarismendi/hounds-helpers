const test = require('ava')
const exception = require('../lib/exception')
const errorMessages = require('../lib/middlewares/error_messages')

test.serial('InternalErrorException', t => {
  const { InternalErrorException } = exception
  t.truthy(InternalErrorException, 'InternalErrorException should exist')
  const error = new InternalErrorException('test')
  t.throws(() => {
    throw error
  }, Error)
  errorMessages(new InternalErrorException('test'), null, null, (err) => {
    t.throws(() => {
      throw err
    }, InternalErrorException)
  })
})

test.serial('BadRequestException', t => {
  const { BadRequestException } = exception
  t.truthy(BadRequestException, 'BadRequestException should exist')
  t.throws(() => {
    throw new BadRequestException('test')
  }, Error)
  errorMessages(new BadRequestException('test'), null, null, (err) => {
    t.throws(() => {
      throw err
    }, BadRequestException)
  })
})

test.serial('UnauthorizedException', t => {
  const { UnauthorizedException } = exception
  t.truthy(UnauthorizedException, 'UnauthorizedException should exist')
  t.throws(() => {
    throw new UnauthorizedException('test')
  }, Error)

  errorMessages(new UnauthorizedException('test'), null, null, (err) => {
    t.throws(() => {
      throw err
    }, UnauthorizedException)
  })
})

test.serial('PaymentException', t => {
  const { PaymentException } = exception
  t.truthy(PaymentException, 'PaymentException should exist')
  t.throws(() => {
    throw new PaymentException('test')
  }, Error)

  errorMessages(new PaymentException('test'), null, null, (err) => {
    t.throws(() => {
      throw err
    }, PaymentException)
  })
})

test.serial('ExpiredException', t => {
  const { ExpiredException } = exception
  t.truthy(ExpiredException, 'ExpiredException should exist')
  t.throws(() => {
    throw new ExpiredException('test')
  }, Error)

  errorMessages(new ExpiredException('test'), null, null, (err) => {
    t.throws(() => {
      throw err
    }, ExpiredException)
  })
})
