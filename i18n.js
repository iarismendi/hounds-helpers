const i18n = require('i18n')
const path = require('path')

module.exports = function (locale) {
  i18n.configure({
    locales: ['es', 'en'],
    directory: `${path.resolve()}/locales`
  })
  i18n.setLocale(locale || 'es')
}
